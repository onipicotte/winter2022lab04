import java.util.Scanner;

public class Shop {
    public static void main(String[] arg){
        Scanner key = new Scanner(System.in);
        Apple crate[] = new Apple[4];

        for(int i = 0; i < crate.length; i++){
            int j = i+1;

            System.out.println("Enter the color of the apple number " + j);
            String color = key.next();
            System.out.println("Enter the size of the apple number " + j);
            int size = key.nextInt();
            System.out.println("Enter the how old in days is the apple number " + j);
            int old = key.nextInt();
            
            Apple apple = new Apple(color, size, old);

            crate[i] = apple;
        }

        System.out.println(crate[3].getColor());
        System.out.println(crate[3].getSize());
        System.out.println(crate[3].getOld());

        System.out.println("Enter the color of the last apple");
        String color = key.next();
        crate[3].setColor(color);
        System.out.println("Enter the size of the last apple");
        int size = key.nextInt();
        crate[3].setSize(size);
        System.out.println("Enter the how old in days is the last apple");
        int old = key.nextInt();
        crate[3].setOld(old);

        System.out.println(crate[3].getColor());
        System.out.println(crate[3].getSize());
        System.out.println(crate[3].getOld());
    }
}