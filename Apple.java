public class Apple{
    private String color;
    private int size;
    private int old;
    private boolean fresh;

    public void isNotFresh(){
        if(this.old > 5){
            this.fresh = false;
        }
    }
    public String getColor() {
        return this.color;
    }
    public int getSize() {
        return this.size;
    }
    public int getOld() {
        return this.old;
    }
    public boolean getFresh() {
        return this.fresh;
    }

    public void setColor(String color) {
        this.color = color;
    }
    public void setSize(int size) {
        this.size = size;
    }
    public void setOld(int old) {
        this.old = old;
    }
    public void setFresh(Boolean fresh) {
        this.fresh = fresh;
    }

    public Apple(String color, int size, int old) {
        this.color = color;
        this.size = size;
        this.old = old;
    }
}

